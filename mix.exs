defmodule Reproduce.MixProject do
  use Mix.Project

  def application, do: []

  def project,
    do: [
      app: :reproduce,
      deps: [],
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      version: "0.0.1-first-april-rc.0"
    ]
end
