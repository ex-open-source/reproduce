defmodule Reproduce do
  def example(first, second), do: String.downcase(first) == second
end
